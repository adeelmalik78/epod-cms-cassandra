--liquibase formatted sql

--changeset adeel:01-01-01
CREATE TABLE person (foo VARCHAR, PRIMARY KEY (foo))
--rollback DROP TABLE person;

--changeset adeel:01-01-02
CREATE TABLE company (foo2 VARCHAR, PRIMARY KEY (foo2))
--rollback DROP TABLE company;

--changeset adeel:01-01-03
CREATE TABLE orders (foo2 VARCHAR, PRIMARY KEY (foo2))
--rollback DROP TABLE orders;

--changeset adeel:01-01-04
CREATE TABLE liquibase.team_average (
   team_name text, 
   cyclist_name text, 
   cyclist_time_sec int, 
   race_title text, 
   PRIMARY KEY (team_name, race_title,cyclist_name));
INSERT INTO liquibase.team_average (team_name, cyclist_name, cyclist_time_sec, race_title) VALUES ('UnitedHealthCare Pro liquibase Womens Team','Katie HALL',11449,'Amgen Tour of California Women''s Race presented by SRAM - Stage 1 - Lake Tahoe > Lake Tahoe');
INSERT INTO liquibase.team_average (team_name, cyclist_name, cyclist_time_sec, race_title) VALUES ('UnitedHealthCare Pro liquibase Womens Team','Linda VILLUMSEN',11485,'Amgen Tour of California Women''s Race presented by SRAM - Stage 1 - Lake Tahoe > Lake Tahoe');
INSERT INTO liquibase.team_average (team_name, cyclist_name, cyclist_time_sec, race_title) VALUES ('UnitedHealthCare Pro liquibase Womens Team','Hannah BARNES',11490,'Amgen Tour of California Women''s Race presented by SRAM - Stage 1 - Lake Tahoe > Lake Tahoe');
INSERT INTO liquibase.team_average (team_name, cyclist_name, cyclist_time_sec, race_title) VALUES ('Velocio-SRAM','Alena AMIALIUSIK',11451,'Amgen Tour of California Women''s Race presented by SRAM - Stage 1 - Lake Tahoe > Lake Tahoe');
INSERT INTO liquibase.team_average (team_name, cyclist_name, cyclist_time_sec, race_title) VALUES ('Velocio-SRAM','Trixi WORRACK',11453,'Amgen Tour of California Women''s Race presented by SRAM - Stage 1 - Lake Tahoe > Lake Tahoe');
INSERT INTO liquibase.team_average (team_name, cyclist_name, cyclist_time_sec, race_title) VALUES ('TWENTY16 presented by Sho-Air','Lauren KOMANSKI',11451,'Amgen Tour of California Women''s Race presented by SRAM - Stage 1 - Lake Tahoe > Lake Tahoe');
--rollback DROP TABLE liquibase.team_average;

